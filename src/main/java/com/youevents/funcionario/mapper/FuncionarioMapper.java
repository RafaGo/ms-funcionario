package com.youevents.funcionario.mapper;

import com.youevents.funcionario.dto.FuncionarioDTO;
import com.youevents.funcionario.model.Funcionario;

public class FuncionarioMapper {

    public static Funcionario dtoToModel(FuncionarioDTO dto) {
        Funcionario novo = new Funcionario();
        novo.setId(dto.getId());
        novo.setAdmissao(dto.getAdmissao());
        novo.setCargo(dto.getCargo());
        novo.setCpf(dto.getCpf());
        novo.setDemissao(dto.getDemissao());
        novo.setEmail(dto.getEmail());
        novo.setEndereco(dto.getEndereco());
        novo.setNome(dto.getNome());
        novo.setTelefone(dto.getTelefone());
        novo.setSalario(dto.getSalario());
        return novo;

    }

    public static FuncionarioDTO modelToDto(Funcionario model) {
        FuncionarioDTO novo = new FuncionarioDTO();
        novo.setId(model.getId());
        novo.setAdmissao(model.getAdmissao());
        novo.setCargo(model.getCargo());
        novo.setCpf(model.getCpf());
        novo.setDemissao(model.getDemissao());
        novo.setEmail(model.getEmail());
        novo.setEndereco(model.getEndereco());
        novo.setNome(model.getNome());
        novo.setTelefone(model.getTelefone());
        novo.setSalario(model.getSalario());
        return novo;

    }
}
