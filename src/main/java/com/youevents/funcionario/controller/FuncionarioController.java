package com.youevents.funcionario.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.youevents.funcionario.dto.FuncionarioDTO;
import com.youevents.funcionario.service.FuncionarioService;
import org.springframework.web.bind.annotation.PutMapping;

@RestController
@RequestMapping("/funcionarios")
public class FuncionarioController {

    @Autowired
    FuncionarioService service;

    // Controle devolve uma responseentity para o postman e dentro vai haver uma
    // lista de dtos.
    @GetMapping
    public ResponseEntity<List<FuncionarioDTO>> listarTodos() {
        List<FuncionarioDTO> lista = this.service.listarTodos();
        return ResponseEntity.ok(lista);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FuncionarioDTO> buscar(@PathVariable String id) throws Exception {
        FuncionarioDTO busca = this.service.buscar(id);
        return ResponseEntity.ok(busca);
    }

    @PostMapping
    public ResponseEntity<FuncionarioDTO> inserir(@RequestBody FuncionarioDTO dto) throws URISyntaxException {
        FuncionarioDTO salvo = this.service.inserir(dto);
        return ResponseEntity.created(new URI("/funcionarios")).body(salvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<FuncionarioDTO> atualizar(@PathVariable String id, @RequestBody FuncionarioDTO dto)
            throws Exception {
        FuncionarioDTO atualiza = this.service.atualizar(id, dto);
        return ResponseEntity.ok(atualiza);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletar(@PathVariable String id) {
        this.service.delete(id);
        return ResponseEntity.noContent().build();
        // oi
    }
}
