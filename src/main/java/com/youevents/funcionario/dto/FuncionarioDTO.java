package com.youevents.funcionario.dto;

import lombok.Data;

@Data
public class FuncionarioDTO {

    private String id;
    private String nome;
    private String endereco;
    private String email;
    private String telefone;
    private String cpf;
    private double salario;
    private String cargo;
    private String admissao;
    private String demissao;

}
